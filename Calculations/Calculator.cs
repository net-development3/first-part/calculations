﻿using System;

namespace Calculations
{
    public static class Calculator
    {
        public static double GetSumOne(int n)
        {
            double sum = 0;

            for (int i = 1; i <= n; i++)
            {
                sum += 1.0 / (double)i;
            }

            return sum;
        }
        
        public static double GetSumTwo(int n)
        {
            double sum = 0;

            for (int i = 1; i <= n; i++)
            {
                sum += (double)Math.Pow(-1.0, i + 1) / (double)(i * (i + 1.0));
            }

            return sum;
        }
        
        public static double GetSumThree(int n)
        {
            double sum = 0;

            for (int i = 1; i <= n; i++)
            {
                sum += 1.0 / (double)Math.Pow(i, 5);
            }

            return sum;
        }
        
        public static double GetSumFour(int n)
        {
            double sum = 0;

            for (int i = 1; i <= n; i++)
            {
                sum += 1.0 / Math.Pow((2 * i) + 1, 2);
            }

            return sum;
        }

        public static double GetProductOne(int n)
        {
            double sum = 1; 

            for (int i = 1; i <= n; i++)
            {
                sum *= 1.0 + (1.0 / Math.Pow(i, 2));
            }

            return sum;
        }
        
        public static double GetSumFive(int n)
        {
            double sum = 0;

            for (int i = 1; i <= n; i++)
            {
                sum += Math.Pow(-1.0, i) / (double)((2 * i) + 1);
            }

            return sum;
        }

        public static double GetSumSix(int n)
        {
            double x1 = 1, x2 = 0, sum = 0;

            for (int i = 1; i <= n; i++)
            {
                x1 *= i;
                x2 += 1.0 / (double)i;
                sum += x1 / x2;
            }

            return sum;
        }

        public static double GetSumSeven(int n)
        {
            double sum = 0;

            for (int i = 1; i <= n; i++)
            {
                sum = Math.Sqrt(2.0 + sum);
            }

            return sum;
        }
        
        public static double GetSumEight(int n)
        {
            double sum = 0, x = 0;

            for (int i = 1; i <= n; i++)
            {
                x += Math.Sin((Math.PI / 180) * i);
                sum += 1.0 / x;
            }

            return sum;
        }
    }
}
